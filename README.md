This is a Wealth Management Web-Application System which allows the wealth managers to advice customers about the models they are offering for a better investment of their money.
Using CRUD, user can:
*Create/list/edit/delete models.
*Create/list/edit/delete securities.
*Assign/remove securities to/from a model.
*Edit the percentage/weighting of securities in a model.
Technologies used are: MVC/3-­tier methodology,
•	MySQL 
•	Java
•	Hibernate
•	REST
•	Tomcat 7
•	Knockout
•	HTML5
•	JQuery
•	Ajax
•	CSS3
•	Bootstrap